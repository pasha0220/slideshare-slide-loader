# -*- coding: utf-8
__author__ = 'pavel'

'''
Slideshare loader
'''

from sys import argv
from os import remove
from lxml import html
from random import getrandbits
import requests
import codecs

from fpdf import FPDF

PATH = '''/home/pavel/code/slideshareLoader/digital-video-interface-comparison-for-global-security-applications'''

class RequestMock:
    class RequestResult:
        status_code = 200
        content = ""

    def get(self, page = "", cookies ={} , timeout = 0):
        with codecs.open(PATH, 'r') as f:
            r = RequestMock.RequestResult()
            r.status_code = 200
            r.content = "".join(f.readlines())
            return r



def main(agrv = list()):
    if len(argv) < 2:
        print("Error. No slide url specified. Args are:")
        for x in  argv:
            print("\t" + x)
        return
    title, imgs , desc = getPageData(argv[-1])
    print(u"Title is {}".format(title))
    count  = len(imgs)

    pdf = FPDF("L", "mm", "A4")
    i = 0
    cookie = {}
    for x in imgs:
        print("Loading image №{} from {}".format(i+1, count))
        i += 1
        r = requests.get(x, cookies=cookie, timeout=30)
        if r.status_code != 200:
            print("\tLoad failed")
            continue
        cookie = r.cookies
        filename = "%s%0x.jpg" %(title, getrandbits(40))
        with open(filename, 'wb') as f:
            f.write(r.content)
        pdf.add_page()
        pdf.image(filename, 0, 0, 300, 210)
        remove(filename)
    pdf.output(u"{}.pdf".format(title), "F")


def getPageData(page):
    #requests = RequestMock()
    r = requests.get(page, cookies={}, timeout=30)
    if r.status_code != 200:
        return
    dom = html.document_fromstring(r.content)
    title = dom.xpath("//h3[contains(@class, transcript-header)]/text()")[-1].strip()
    imgs = dom.xpath("//section[contains(@class,'slide')]/img/@data-full")
    desc = dom.xpath("//ol[contains(@class, 'j-transcripts transcript')]/li")
    return title, imgs, desc


if __name__ == "__main__":
    main()
else:
    print("Not a package")
